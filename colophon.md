* Hardware:
	- Macbook 13"
	- Macbook 15"
	- Dell notebook 15"
* Color management:
	- Color aid chart ([vide](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/issues/29/workflow-color-management-chart))
* Software:
    - Environmental setting up (initial)
    	- [Talus](https://github.com/juanbrujo/Talus): `bash` script to automate download & setup your Mac OS X development environment
    - [Duplicati](https://www.duplicati.com/) (open-source backup software)
	- [ImageOptim](https://github.com/ImageOptim/ImageOptim): image optimization
	- [Filmulator](https://github.com/CarVac/filmulator-gui): RAW format editor
	- [RAWStudio](https://github.com/rawstudio/rawstudio): RAW file format editor
	- [ScanBot](https://scanbot.io/en/index.html): scan, edit and manage all of your paperwork
	- [The Photographer's Ephemeris](https://itunes.apple.com/app/id366195670): light condition timetable
	- [Scan Tailor](http://scantailor.org/): post-processing tool for scanned pages: (page splitting, deskewing, adding/removing borders...)
	- Bibliographic searcher:
        - [ScienceFair](http://sciencefair-app.com): Discover, collect, organise, read and analyse scientific papers
	- Terminal:
        - [Hyper](https://hyper.is/): Terminal console for the MacOSX
* Online tools:
    - [Plantuml](http://www.plantuml.com/plantuml/uml/):  Diagram / deployment diagram / critical path 