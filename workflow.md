## Preliminar (originally from `Bibliography.md`)
* Se utilizó luz natural, entre las 9 y 11 horas durante el correr del proceso. 
* detección de errores (generar un lista de verificación _aka_ checklist) se buscará la redundancia en los sistemas de resguardo de la información generación de perfiles personalizados acordes al sofware y hardware utilizado. 
* No se contempla la aplicación de la función OCR (reconocimiento óptico de caracteres) almacenamiento (dispositivos y medios): política de copia de seguridad. 
* Mantenimiento (copias de seguridad, migración, preservación y seguridad) Sistema único de nombre de archivos (nombre del proyecto, número de página con números arábigos más la extensión elegida: `TIFF` en nuestro caso, primariamente). 
* El nombre del archivo no deberá tener más de 256 caracteres (incluyendo la extensión de archivo: `.RAW`), sea cual sea el sistema operativo.
* Se busca compatibilidad con todos los sistemas operativos y sus juegos de caracteres afines. formato LZW (compresión [Lizzardtech](https://en.wikipedia.org/wiki/MrSID=) 
* crear perfiles personalizados de cámara, 
* crear perfiles de manejo del color,
* agregar a cada scan un perfil de color
* crear perfiles de LUT, 
* Buscar perfiles de Adobe Lightroom para la cámara fotográfica
* Unicode character set for all the names of files (incompatibilities issues between OSs and between softwares)
* durante todo el proceso, utilizar el formato [.RAW](https://es.wikipedia.org/wiki/Raw_(formato)) como formato de archivo.