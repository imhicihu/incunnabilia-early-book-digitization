* Digitization in the Real World, Kwong Bor Ng & Jason Kucsma; Metropolitan New York Library Council (October 18, 2010); 592 pp. (reading...)
     * at first time, seems an outdated book: "(...) templates available in FrontPage and Kompozer felt limiting." (p. 15)
     * 
     
     
     
buscar referencia (rescatado hoy desde el archivo `colophon.md` en el escritorio de la Macbook Air)
El¡Se acabó el papel! Ferreyra, Carlos Alfredo (compilador), 202 pp, 2010, ISBN 978-987-26275-0-8
Centro Regional de Preservación y Conservación del Patrimonio Cultural en Obras sobre Papel, 2010
Regionalista, "(...) este libro permitirá que los cordobeses que están comprometidos en la lucha cotidiana por la salvaguarda del patrimonio cultural (...)"
"(...) a la limpieza del material antiguo..."
"agentes biológicos, ambientales y humanos que producen deterioro en estos soportes y que se sugiere para el manejo de estas plagas (...)

Capítulo: Digitalización de archivos, fotografías y documentos (Marcelo Rico)
"Un archivo es la huella documental de lo que el hombre hizo".
"(...) la estrategia de la UNESCO del conocimiento para todos. También tienen una estrecha relación con el Programa "Memoria del Mundo" de la UNESCO cuyo objetivo es ala salvaguarda del patrimonio documental internacional, el acceso democrático a dicho patrimonio, el conocimiento creciente de su significado y la necesidad de preservarlo."
"(...) puesto que la digitalización supone un trabajo intenso y caro es importante capturar una imagen de modo que sea posible utilizarla paa satisfacer diferentes necesidades".
"(...) La captura de imágenes digitales debe tomar en cuenta los procesos técnicos comprendidos al convertir una representación analógica en digital, así como también los atributos de los documentos fuente en sí mismos: dimensiones físicas y representación, nivel de detalles, rango tonal y presencia de color."
Procesos:
Creación de archivos: captura o conversión inicial de un documento u objeto a la forma digital, por lo general con un escáner o cámara digital.
Gestión de archivos: organización, almacenamiento y mantenimiento de imágenes y metadatos relacionados.
La entrega de la imagen: comprende el proceso de hacer llegar las imágenes al usuario y abarca redes, dispositivos de visualización e impresoras.
Integración del sistema:
"(...) mantenga la cantidad de dispositivos al mínimo."
"(...) elija productos que cumplan con las normas y que tengan una amplia aceptación en el mercado y un fuerte apoyo por parte del proveedor."
"(...) operaciones de procesamiento de imágenes: eliminación de muaré (de-screening), eliminación de puntos (despeckling), eliminación de oblicuidad (deskewing), aumento de nitidez (sharpening), compresión aplicada a los archivos digitales, escala aplicada a (todos) los archivos, 
"(...) creación y anexo de metadatos a cada uno de los activos digitales: organización, rastreo y descripción técnica de los archivos y su posterior tratamiento sea por lotes o de manera individual.
"(...) Razones para crear un original digital enriquecido:  Existen imperiosos motivos de preservación, acceso y económicos  para crear un archivo maestro de imagen digital enriquecido (algunas veces denominado imagen para archivo) en el cual se representa toda la información importantes que contiene el documento fuente." "(...) Persigue tres objetivos: protección de originales vulnerables, reemplazo de originales y preservación de archivos digitales." 
"(...) Categorías de metadatos: descriptivos, estructurales y administrativos
Bibliografía:
Canadian conservation institute.
Haley, Alan, Yasmeen Khan, Andrew Robb, Ann Seibert and Mary Wootton, 1999. Conservation implications of digitization projects. Washington CD Library of Congress.
Journal of The American Institute for conservation
Universidad de Cornell. Tutorial de digitalización
IFLA Core Programme Preservation and conservation. International Federation of library associations and institute.






Colophon
Se utilizó luz natural, entre las 9 y 11 horas durante el correr del proceso.
detección de errores (generar una checklist)
se buscará la redundancia en los sistemas de resguardo de la información
generación de perfiles personalizados acordes al sofware y hardware utilizado.
No se contempla la aplicación de la función OCR (reconocimiento óptico de caracteres)
almacenamiento (dispositivos y medios): política de copia de seguridad.
Mantenimiento (copias de seguridad, migración, preservación y seguridad)
Sistema único de nombre de archivos (nombre del proyecto, número de página con números arábigos más la extensión elegido: TIFF en nuestro caso, primariamente). No deberá el nombre los 256 caracteres (incluyendo la extensión de archivo: *.TIFF). Se busca compatibilidad con todos los sistemas operativos y sus juegos de caracteres afines.
formato LZW (compresión Lizzardtech)
crear perfiles personalizados de cámara, de manejo del color, de LUT, 
Buscar perfiles de Adobe Lightroom para la cámara fotográfica


Zuleta, Juan Antonio, 1956- La imagen digital sin misterios. Buenos Aires, Ediciones Fotográficas Argentina, 2000. Ubicación: B. 7061 ¦ CDR 328 (T): (p. 11) Rango dinámico: es la diferencia entre la densidad máxima (sombras más densas) y la densidad mínima (altas luces más intensas) en una imagen. Se mide en unidades de densidad. A menudo se utilizan indistintamente los términos rangos dinámicos y contraste, lo cual es un error. 


The 2nd UNESCO Regional Training Workshop on the Preservation of and access to the Documentary Heritage in Asia and the Pacific Region. Mmemory of the world. Digitalization of documentary heritage for preservation and acces. Cheongju, Korea, 4-7 september 2004.
(...) A significant part of the digital heritage consists of the product of the digital reproduction of pre-existing work,s, which may consist of texts, images, sounds, or which may be of an audiovisual, graphic, photographic or cinematographic nature, etc., recorded on a defined, stable material medium. This digital "double" does not claim to be an identical copy of the initial work, but contents itself with being a representation of it: it is a snapshot, a print, a trace at a given moment in time, and in every case, the result of a voluntary policy of digitization. (p. 57)
(...) UNESCO's campaign (...) The scope and ambition of the Guidelines are constrained (...) it was decide to adopt a principles approach that might serve as a checklist of issues and possibilities that programmes need to take into account. 

The present condition & preservation measures for the documentary heritage in Korea
National History compilation commission (Korea)
The National Culure Promotion Committee (korea)
Gyujang-gak of Seoul National University
National Cultural Properties Research Institute (Korea)
Korean Studies Promotion Institute
Goryo Daejanggyeong Research Institute (the complete collection of Buddhist Sutras)---<9 www.sutra.re.kr
UNESCO Memory of the world digitization project
yemenite manuscripts
Egyptian National library
memory of russia project


(...) The printing blocks and the haning board of the Gyujang-gak have been photographed and then made into 3D (3-dimensions) so that the details of the condition of their preservation, their structural characteristics and the letter types are clearly shown. (p. 100)
(...) These records have now gecome very old and brittle. Writings of some of these records have become indistintct and many records have turned so brittle  that they cold not be handled. Fungus and insects have attacked a lot of the recorrds. Some of these records if severely damaged, could not be replaced, because these are the only copies in the world. So that the presevation process using computer has been started." (p. 118)





LLEGUE HASTA LA PAGINA 120 of The 2nd UNESCO REGIONAL TRAINING WORKSHOP----) Memory of the world