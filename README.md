![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![Bitbucket issues](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)
![internal-wip](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)


# Rationale 

* This repository shows the process-workflow of digitization of an _incunabulae_.

![scanning.gif](https://i.ibb.co/bK22xGM/ezgif-2-4b2831b23367.gif)

* This is a living document that will grow and change over time.

### What is this repository for? ###

* _Process-workflow-description_ turning a book (material object) and becoming a _digital asset_. 
* Version 1.3

### How do I get set up? ###

* Summary of set up
    - There is a preliminary document about software. Can be found [here](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/issues/3/software)
    - Implies [hardware and software](https://bitbucket.org/imhicihu/incunnabilia-early-book-digital/issues/1/hardware-camera-lens): camera, lens, lux control, image editors, creation of custom profiles (for optimization and a smooth workflow between hardware & software), etc.
    - Gathering of data of possible-scenarios/environment or even better: leading cases (through forums, digital libraries, bibliography, etc.). Hence, a private repository of bibliography will _become_ an objective.
    - There is a Bibliography section. Can be found [here](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/src/b13740330d22c031dab8f8cd69bde3b73baf8ac9/Biblioteca_Congreso.md?at=master&fileviewer=file-view-default) (in progress)
    - There is a Links section. Can be found [here](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/src/9219cd057c533589234365b3669c32e016ba01bf/Links.md?at=master&fileviewer=file-view-default)
* Configuration
    - Up to now, in the process of gathering data to meet international norms. It a _mixture_ between [hardware](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/issues?q=hardware) & [software](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/issues/3/software)
* Dependencies
    - The same nature of the process, needs a _myriad_ of steps done consecutively to reach the goal. Check this repository for more data about software, hardware and good practices
* How to run tests
    - Check our workflow and our software & hardware files offered inside this repo
* Deployment instructions
    - There is a [workflow](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/src/master/workflow.md) offered

### Related repositories ###

* Some repositories linked with this project:
     - [Digitalizacion (worflow)](https://bitbucket.org/imhicihu/digitalizacion-worflow/src/)
     - [Migration data](https://bitbucket.org/imhicihu/migration-data-checklist/src/)
     - [AI Document recognition](https://bitbucket.org/imhicihu/ai-document-recognition/src/)
     - [PDF inner structure](https://bitbucket.org/imhicihu/pdf-inner-structure/src/master/src/)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/issues) 

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/commits/) section for the current status

### Contribution guidelines ###

* Writing tests
    - No data to share (up to now)
* Code review
    - No data to share (up to now)
* Other guidelines
    - Check the _[issues](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/issues)_

### Who do I talk to? ###

* Repo owner or admin
     - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
     - Our [Board](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/addon/bitbucket-trello-addon/trello-board) is enabled and open to any inquiry. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)