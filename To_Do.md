### Miscellaneous ###
* Choose a licence and put it on README (chooselicence.org)
* Buscar perfiles de Adobe Lightroom para la c�mara fotogr�fica
* crear perfiles personalizados de c�mara, de manejo del color, de LUT, 
* Fill the gaps in `Colophon.md` file (both sections)
* pdf viewer (experimental) > https://github.com/mozilla/pdf.js
* ~~Migrate Del.icio.us gathered links to Evernote~~
### Image Viewer (PDF, Jpeg file format)
* [Universal Viewer](https://github.com/universalviewer/universalviewer)
* [Wellcome viewer](https://github.com/wellcometrust/player)
* [OpenSeaDragon](https://openseadragon.github.io/)
* [Mirador](https://github.com/ProjectMirador/mirador)
* [DivaJS](https://ddmal.music.mcgill.ca/diva.js/)
* [InternetBookReader](https://github.com/internetarchive/bookreader)
* [React PDF viewer](https://react-pdf-viewer.dev/)